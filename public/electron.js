const electron = require('electron');
const {app, globalShortcut, Menu, BrowserWindow, Tray, ipcMain, clipboard} = electron
const isDev = require('electron-is-dev');
const path = require('path');

let mainWindow;

function createWindow() {
    mainWindow = new BrowserWindow({width: 800, height: 600});
    mainWindow.setMenuBarVisibility(false)
    let text = clipboard.readText();
    mainWindow.loadURL(`https://translate.google.com/#view=home&op=translate&sl=en&tl=hu&text=${text}`)
    //mainWindow.loadURL(isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`);
    if(isDev)
        mainWindow.webContents.openDevTools();

    mainWindow.on('closed', () => mainWindow = null)
}

function createTray() {
    tray = new Tray(`${__dirname}/icon.png`)
    const contextMenu = Menu.buildFromTemplate([
      {role: 'quit', label:'Exit'}
    ])
    tray.setToolTip('Devla')
    tray.setContextMenu(contextMenu)
}

ipcMain.on('message', (event, arg) => {
    console.log(`message ${arg} desu`)
    event.sender.send('reply', 'Gyula ba')
})

app.on('ready', () => {
    globalShortcut.register('CommandOrControl+Q', createWindow)
    createWindow()
    createTray()
});

app.on('window-all-closed', function () {
    /*if (process.platform !== 'darwin')
        app.quit()*/
});

app.on('activate', function () {
    if (mainWindow === null)
        createWindow()
});

