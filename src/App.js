import React, { Component } from 'react';
import './App.css';
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography';


const electron = (window.require != undefined) ? window.require("electron"): {
  ipcRenderer : {
    on : ()=> {},
    send : ()=>{}
  }
};
const {ipcRenderer} = electron

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {message : "semmi"}
    ipcRenderer.on("reply", (event, data) => this.setState({message : data}))
  }
  buttonClicked() {
    ipcRenderer.send("message","Gyufi")
  }

  render() {
    return (
      <div className="App">
        <Typography>
          Hello react
        </Typography>
        <Button onClick={this.buttonClicked} variant="contained" color="primary">Gyufi</Button>
        <Typography>
          {this.state.message}
        </Typography>
      </div>
    );
  }
}

export default App;
